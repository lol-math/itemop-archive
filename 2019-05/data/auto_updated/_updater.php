<?php
$patchNoData = json_decode(file_get_contents('https://ddragon.leagueoflegends.com/api/versions.json'));
$patchNo = $patchNoData[0];


updateFile("Riot_Champion_Base_Stats.json", "http://ddragon.leagueoflegends.com/cdn/$patchNo/data/en_US/champion.json");
updateFile("Riot_Items.json", "http://ddragon.leagueoflegends.com/cdn/$patchNo/data/en_US/item.json");
updateFile("Riot_Runes.json", "http://ddragon.leagueoflegends.com/cdn/$patchNo/data/en_US/runesReforged.json");

//updateFile("Runes.json", "https://gitlab.com/lol-math/item-optimizer/raw/master/packages/item-optimizer-data/src/json/runes.json");
//updateFile("Skill_Order.json", "https://gitlab.com/lol-math/item-optimizer/raw/master/packages/item-optimizer-data/src/json/skillOrder.json");
//updateFile("Enemy_Damage_Composition.json", "https://gitlab.com/lol-math/item-optimizer/raw/master/packages/item-optimizer-data/src/json/damageComposition.json");


function updateFile($fileToUpdate, $dataURL){
	$data = fopen($dataURL, 'r');
	if ($data != false){
		file_put_contents($fileToUpdate, $data);
	}
}

//http://ddragon.leagueoflegends.com/cdn/9.5.1/img/champion/Kayle.png   Champion Square Cutouts
?>